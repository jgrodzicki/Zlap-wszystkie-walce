﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowScript : MonoBehaviour {
    
    private GameObject target;
    private NavMeshAgent agent;
    public float speed;

    // Use this for initialization
    void Start () {
        target = GameObject.Find("Player");
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 diff = transform.position - target.transform.position;
        if (diff.sqrMagnitude > 20) return;
        if (diff.sqrMagnitude < 4)
        {
            agent.SetDestination(transform.position);
        }
        else
            agent.SetDestination(target.transform.position);
    }
}
