﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBrain : MonoBehaviour {

    public int NUMBER_OF_OBJECTS;

	// Use this for initialization
	void Start () {
        GameObject prefab = Resources.Load("Sheep") as GameObject;
        for (int i = 0; i < NUMBER_OF_OBJECTS; i++) {
            GameObject gameObject = Instantiate(prefab) as GameObject;
            gameObject.transform.position = new Vector3(3, i * 2, 0);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
