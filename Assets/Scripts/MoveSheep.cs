﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveSheep : MonoBehaviour
{
    public float speed;
    //public Rigidbody rigid;
    private float moveX, moveY, rotationZ;
    NavMeshAgent agent;
    Transform destination;

    // Use this for initialization
    void Start()
    {
        //rigid = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
        if (agent == null)
        {
            Debug.LogError("nie ma navagent playera");
        }
    }

    // Update is called once per frame
    void Update()
    {
        moveX = 0;
        moveY = 0;

        if (Input.GetKey(KeyCode.D))
            moveX += speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
            moveX += -speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.W))
            moveY += speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.S))
            moveY += -speed * Time.deltaTime;

        Vector3 moveVector = new Vector3(moveX, 0f, moveY);
        Debug.Log(moveVector);
        transform.position += moveVector;
        //rigid.position += new Vector3(moveX, moveY, 0f);
        //agent.SetDestination(rigid.position);
    }
}